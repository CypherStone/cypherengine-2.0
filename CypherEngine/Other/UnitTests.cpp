#include "UnitTests.h"

#include "Maths/Vector/Vector_Float.h"
#include "Maths/Vector/Vector_SSE.h"

#include <chrono>
#include <iostream>
#include <time.h>

#if UNIT_TESTS_BUILD

void UnitTests::Algorithms()
{
	Random rand;
	rand.ApplySeed();

	std::vector<int32> test;
	std::vector<int32> test1;

	const int32 size = 1000;
	int32 testWrap = 0;
	testWrap = Maths::BranchlessClamp(testWrap, 5, 10);

	for (int32 i = 0; i < size; ++i)
	{
		test.push_back(rand.GetInt());
		test1.push_back(rand.GetInt());

	}

	Algorithms::Shuffle(test.data(), test.size());
	Algorithms::Shuffle(test1.data(), test1.size());
	
	using namespace std::chrono;

	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	Algorithms::QuickSort(test.data(), test.size(), Algorithms::SimpleCompare<int32>);

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	Algorithms::MergeSort(test1.data(), test1.size(), Algorithms::SimpleCompare<int32>);

	high_resolution_clock::time_point t3 = high_resolution_clock::now();

	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	duration<double> time_span1 = duration_cast<duration<double>>(t3 - t2);

	std::cout << "QuickSort: " << time_span.count() << std::endl;
	std::cout << "MergeSort: " << time_span1.count() << std::endl;

	int32 randomIndex = rand.GetRangeInt(0, size) - 1;
	int32 findData = test1[randomIndex];
	int32 find = Algorithms::BinarySearch(test1.data(), test1.size(), findData, Algorithms::SimpleCompare<int32>);

	assert(find == randomIndex);

	const char* testStr = "Hello";
	const char* fileExtTest = "test.txt";
	char testFile[256] = { '\0' };
	memcpy(testFile, fileExtTest, sizeof(char) * StringUtils::GetLength(fileExtTest));

	int32 length		= StringUtils::GetLength(testStr);
	int32 lPos			= StringUtils::FindFirstOf(testStr, 'H');
	const char* name	= StringUtils::GetFileName(testFile);

	assert(length != -1);
	assert(lPos != -1);

	float rFloat = rand.GetFloat();
	float rDFloat = rand.GetDecimalFloat();
	float rRFloat = rand.GetRangeFloat(10.0f, 100.0f);
}

void UnitTests::Vector()
{
	Random rand;
	rand.ApplySeed();

	const int64 size = 100000;
	
	using namespace std::chrono;
	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	float floatDistance;
	float floatResult;
	for (int64 i = 0; i < size; ++i)
	{
		Vector_Float float1(1.0f, 2.0f, 0.5f, 5.0f);
		Vector_Float float2(-2.0f, 1.5f, 1.0f, 2.0f);

		floatDistance = Vector_Float::Distance(float1, float2);
		floatResult = Vector_Float::Length(float1);

		std::cout << "Float: " << floatDistance << " & " << floatResult << std::endl;
	}

	duration<double> time_span1 = duration_cast<duration<double>>(high_resolution_clock::now() - t1);

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	float sseDistance;
	float sseResult;
	for (int64 i = 0; i < size; ++i)
	{
		Vector_SSE sse1(1.0f, 2.0f, 0.5f, 5.0f);
		Vector_SSE sse2(-2.0f, 1.5f, 1.0f, 2.0f);

		sseDistance = Vector_SSE::Distance(sse1, sse2);
		sseResult = Vector_SSE::Length(sse1);

		std::cout << "SSE: " << sseDistance << " & " << sseResult << std::endl;
	}

	duration<double> time_span2 = duration_cast<duration<double>>(high_resolution_clock::now() - t2);
	std::cout << "Float Implementation: " << time_span1.count() << std::endl;
	std::cout << "SSE Implementation: " << time_span2.count() << std::endl;
}

#endif // UNIT_TESTS_BUILD