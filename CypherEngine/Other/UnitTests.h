#ifndef CE_UNIT_TESTS
#define CE_UNIT_TESTS

#define UNIT_TESTS_BUILD 0

namespace UnitTests
{
	void Algorithms();

	void Vectors();
};

#endif // CE_UNIT_TESTS