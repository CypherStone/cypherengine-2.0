#include "Adapters/AdapterManager.h"
#include "Drivers/Driver.h"
#include "Model/FSM.h"
#include "Platform/Platform.h"
#include "Window/Window.h"
#include "Containers/Algorithms.h"
#include "Maths/Maths.h"
#include "Maths/Random.h"
#include "String/StringUtils.h"
#include "Render/ImGuiSupport.h"

enum class GameFlow
{
	GF_UNKNOWN,
	GF_INIT,
	GF_UPDATE,
	GF_SHUTDOWN
};

int main()
{
	Platform::Initialise();

	Window* mainWindow = Window::Create("MainWindow", 1280, 720);

	if (mainWindow == nullptr)
	{
		return -1;
	}

	ADAPTERMANAGER_S::Instantiate();

	GraphicDriver defaultDriver = Driver::GetDefaultDriver();
	ADAPTERMANAGER.Setup(defaultDriver);

	const Adapter* mainAdapter	= ADAPTERMANAGER.GetMainAdapter();
	Driver* driver				= Driver::Create(mainAdapter, mainWindow);

	GameFlow state				= driver != nullptr ? GameFlow::GF_INIT : GameFlow::GF_SHUTDOWN;
	FSM<GameFlow> stateMachine	= FSM<GameFlow>(GameFlow::GF_INIT);

	ImGuiSupport imgui;
	imgui.Initialise();

	while (stateMachine.GetState() != GameFlow::GF_SHUTDOWN)
	{
		imgui.Update(mainWindow);
		mainWindow->Process();

		ImGui::SetNextWindowPos(ImVec2(640, 310));
		ImGui::Begin("HW");
			ImGui::Text("Hello World");
		ImGui::End();

		g_Driver->StartFrame();

		

		if (stateMachine.GetState() == GameFlow::GF_INIT)
		{

		}
		else if (stateMachine.GetState() == GameFlow::GF_UPDATE)
		{

		}

		stateMachine.Update();
	
		if (mainWindow->IsClosed() == true)
		{
			stateMachine.SetState(GameFlow::GF_SHUTDOWN);
			break;
		}

		imgui.Render();

		g_Driver->EndFrame();
	}

	Window::Destroy(mainWindow);

	ADAPTERMANAGER.Shutdown();
	ADAPTERMANAGER_S::Destroy();

	return 0;
}