#ifndef CE_IMGUI_IMPLEMENTATION
#define CE_IMGUI_IMPLEMENTATION

#define IMGUI_SUPPORT_ENABLED 1

#if IMGUI_SUPPORT_ENABLED

#include "imgui-1.79/imgui.h"

#include "Drivers/D3D12_Defines.h"

class ImGuiSupport
{
public:
	bool	Initialise();
	void	Update(class Window* window);
	void	Render();

private:

	bool	CreateFonts();
	void	SetupRenderState();

	ID3D12RootSignature* g_pRootSignature = nullptr;
	ID3D12PipelineState* g_pPipelineState = nullptr;
	DXGI_FORMAT                  g_RTVFormat = DXGI_FORMAT_UNKNOWN;
	ID3D12Resource* g_pFontTextureResource = nullptr;
	D3D12_CPU_DESCRIPTOR_HANDLE  g_hFontSrvCpuDescHandle = {};
	D3D12_GPU_DESCRIPTOR_HANDLE  g_hFontSrvGpuDescHandle = {};

	struct FrameResources
	{
		ID3D12Resource* IndexBuffer;
		ID3D12Resource* VertexBuffer;
		int                 IndexBufferSize;
		int                 VertexBufferSize;
	};

	FrameResources*		g_pFrameResources = nullptr;
	uint32				g_frameIndex = UINT_MAX;
};

#endif //IMGUI_SUPPORT_ENABLED

#endif //CE_IMGUI_IMPLEMENTATION
