#ifndef CE_MATHS_H
#define CE_MATHS_H

#include "Includes.h"

#define AS_BIT(x) 1 << x

namespace Maths
{
	// A temporary variable-less numeric XOR swap.
	template <class T>
	void SwapNumericXOR(T& a, T& b)
	{
		if (a == b)
		{
			return;
		}

		a = a ^ b;
		b = a ^ b;
		a = a ^ b;
	}

	// A temporary variable-less numeric addition arithmetic swap.
	template <class T>
	void SwapNumericAdd(T& a, T& b)
	{
		if (a == b)
		{
			return;
		}

		a = a + b;
		b = a - b;
		a = a - b;
	}

	// A temporary variable-less numeric multiplication arithmetic swap.
	template <class T>
	void SwapNumericMul(T& a, T& b)
	{
		if (a == b)
		{
			return;
		}

		a = a * b;
		b = a / b;
		a = a / b;
	}

	// Wraps a number between a min and max.
	template <class T>
	T Wrap(T in, T min, T max)
	{
		return min + (in % (max - min));
	}

	// Clamp the input based on the min and max for the output.
	template <class T>
	T Clamp(const T a, const T min, const T max)
	{
		return a > max ? max : a > min ? a : min;
	}

	template <class T>
	T BranchlessClamp(const T a, const T min, const T max)
	{
		return Wrap(a, min, max);
	}

	// Convert the input into a range [0.0f-1.0f].
	template <class T>
	float Range(const T a, const T min, const T max)
	{
		return float(a - min) / float(max - min);
	}

	// Convert the clamped input into a range [0.0f-1.0f].
	template <class T>
	float ClampRange(const T a, const T min, const T max)
	{
		return float(Clamp(a, min, max) - min) / float(max - min);
	}

	// Convert the clamped input into a remapped range [0.0f-1.0f] * the specified output min and max.
	template <class T, class O>
	O MapRange(const T a, const T min, const T max, const O outMin, const O outMax)
	{
		return outMin + O(ClampRange(a, min, max) * (outMax - outMin));
	}

	template <class T>
	bool IsClose(T a, T b, T epsilon)
	{
		return (a - b) < epsilon;
	}

	template <class T>
	bool IsNearZero(T v, T epsilon)
	{
		return IsClose(v, T(0), epsilon);
	}
};

#endif // CE_MATHS_H