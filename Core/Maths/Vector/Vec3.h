#ifndef CE_VEC3_H
#define CE_VEC3_H

#include "Vector.h"
#include "Vector_Base.h"

class Vec3
{
public:
	VECTOR_IMPLEMENTATION(Vec3);

private:
	VectorImp	m_vector;
};

#endif // CE_VEC3_H