#ifndef CE_VECTOR_SSE_H
#define CE_VECTOR_SSE_H

#include "Vector_Base.h"
#include "Intrinsics.h"

class Vector_SSE
{
public:
	VECTOR_IMPLEMENTATION(Vector_SSE);

private:

	float128	m_components;
};

#endif // CE_VECTOR_SSE_H