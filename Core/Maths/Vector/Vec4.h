#ifndef CE_VEC4_H
#define CE_VEC4_H

#include "Vector.h"
#include "Vector_Base.h"

class Vec4
{
public:
	VECTOR_IMPLEMENTATION(Vec4);

private:
	VectorImp	m_vector;
};

#endif // CE_VEC4_H