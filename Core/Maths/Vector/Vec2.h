#ifndef CE_VEC2_H
#define CE_VEC2_H

#include "Vector.h"
#include "Vector_Base.h"

class Vec2
{
public:
	VECTOR_IMPLEMENTATION(Vec2);

private:
	VectorImp	m_vector;
};

#endif // CE_VEC3_H