#include "Vector_SSE.h"

#include <math.h>
#include "Maths/Maths.h"

Vector_SSE::Vector_SSE()
{
	m_components = Intrinsics::Set(0.0f);
}

Vector_SSE::Vector_SSE(float x, float y, float z, float w)
{
	m_components = Intrinsics::SetAll(x, y, z, w);
}

void Vector_SSE::GetData(float* data) const
{
	Intrinsics::Store(this->m_components, data);
}

float Vector_SSE::Dot(const Vector_SSE& b) const
{
	return Dot(*this, b);
}

Vector_SSE Vector_SSE::Cross(const Vector_SSE& b) const
{
	return Cross(*this, b);
}

void Vector_SSE::Normalize()
{
	*this = Normalize(*this);
}

void Vector_SSE::NormalizeSafe()
{
	*this = NormalizeSafe(*this);
}

float Vector_SSE::Legnth() const
{
	return Length(*this);
}

float Vector_SSE::Legnth2() const
{
	return Length2(*this);
}

float Vector_SSE::Distance(const Vector_SSE& b) const
{
	return Distance(*this, b);
}

float Vector_SSE::Distance2(const Vector_SSE& b) const
{
	return Distance2(*this, b);
}

Vector_SSE Vector_SSE::operator-(const Vector_SSE& b) const
{
	Vector_SSE o;
	o.m_components = Intrinsics::Subtract(this->m_components, b.m_components);
	return o;
}

Vector_SSE Vector_SSE::operator+(const Vector_SSE& b) const
{
	Vector_SSE o;
	o.m_components = Intrinsics::Add(this->m_components, b.m_components);
	return o;
}

Vector_SSE Vector_SSE::operator*(const Vector_SSE& b) const
{
	Vector_SSE o;
	o.m_components = Intrinsics::Multiply(this->m_components, b.m_components);
	return o;
}

Vector_SSE Vector_SSE::operator/(const Vector_SSE& b) const
{
	Vector_SSE o;
	o.m_components = Intrinsics::Divide(this->m_components, b.m_components);
	return o;
}

Vector_SSE Vector_SSE::operator/(const float b) const
{
	Vector_SSE o;
	o.m_components = Intrinsics::Divide(this->m_components, Intrinsics::Set(b));
	return o;
}

Vector_SSE Vector_SSE::operator*(const float b) const
{
	Vector_SSE o;
	o.m_components = Intrinsics::Multiply(this->m_components, Intrinsics::Set(b));
	return o;
}

float Vector_SSE::Dot(const Vector_SSE& a, const Vector_SSE& b)
{
	return Intrinsics::Dot(a.m_components, b.m_components);
}

Vector_SSE Vector_SSE::Cross(const Vector_SSE& a, const Vector_SSE& b)
{
	Vector_SSE o;
	o.m_components = Intrinsics::Cross(a.m_components, b.m_components);
	return o;
}

Vector_SSE Vector_SSE::Normalize(const Vector_SSE& a)
{
	float length = Length(a);

	Vector_SSE o(a);
	return o / length;
}

Vector_SSE Vector_SSE::NormalizeSafe(const Vector_SSE& a)
{
	float length = Length(a);

	Vector_SSE o(a);
	if (Maths::IsNearZero(length, 0.001f) == false)
	{
		return o / length;
	}
	return o;
}

float Vector_SSE::Length(const Vector_SSE& a)
{
	return Intrinsics::Length(a.m_components);
}

float Vector_SSE::Length2(const Vector_SSE& a)
{
	return Intrinsics::Dot(a.m_components, a.m_components);
}

float Vector_SSE::Distance(const Vector_SSE& a, const Vector_SSE& b)
{
	return Intrinsics::Distance(a.m_components, b.m_components);
}

float Vector_SSE::Distance2(const Vector_SSE& a, const Vector_SSE& b)
{
	return Intrinsics::Distance2(a.m_components, b.m_components);
}
