#ifndef CE_VECTOR_H
#define CE_VECTOR_H

#define CE_USE_VECTOR_FLOAT 1
#define CE_USE_VECTOR_SSE 0

#if CE_USE_VECTOR_FLOAT
#include "Maths/Vector/Vector_Float.h"
typedef Vector_Float			VectorImp;
#elif CE_USE_VECTOR_SSE
#include "Maths/Vector/Vector_SSE.h"
typedef Vector_SSE				VectorImp;
#endif // CE_USE_VECTOR_SEE

#endif // CE_VECTOR_H