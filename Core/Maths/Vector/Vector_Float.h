#ifndef CE_VECTOR_FLOAT_H
#define CE_VECTOR_FLOAT_H

#include "Vector_Base.h"

class Vector_Float
{
public:
	VECTOR_IMPLEMENTATION(Vector_Float);

private:

	union
	{
		struct { float m_x, m_y, m_z, m_w; };
		float m_components[4];
	};
};

inline void Vector_Float::GetData(float* data) const
{
	data[0] = m_components[0];
	data[1] = m_components[1];
	data[2] = m_components[2];
	data[3] = m_components[3];
}

#endif // CE_VECTOR_FLOAT_H
