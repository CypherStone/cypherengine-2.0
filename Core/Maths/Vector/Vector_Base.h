#ifndef CE_VECTOR_BASE_H
#define CE_VECTOR_BASE_H

#include "Includes.h"

#define VECTOR_IMPLEMENTATION(class_name)							\
class_name();														\
class_name(float x, float y, float z, float w);						\
void 					GetData(float* data) const;					\
float					Dot(const class_name& b) const;				\
class_name				Cross(const class_name& b) const;			\
void					Normalize();								\
void					NormalizeSafe();							\
float					Legnth() const;								\
float					Legnth2() const;							\
float					Distance(const class_name& b) const;		\
float					Distance2(const class_name& b) const;		\
class_name				operator-(const class_name& b) const;		\
class_name				operator+(const class_name& b) const;		\
class_name				operator/(const class_name& b) const;		\
class_name				operator*(const class_name& b) const;		\
class_name				operator/(const float b) const;				\
class_name				operator*(const float b) const;				\
																	\
static float			Dot(const class_name& a, const class_name& b);			\
static class_name		Cross(const class_name& a, const class_name& b);		\
static class_name		Normalize(const class_name& a);							\
static class_name		NormalizeSafe(const class_name& a);						\
static float			Length(const class_name& a);							\
static float			Length2(const class_name& a);							\
static float			Distance(const class_name& a, const class_name& b);		\
static float			Distance2(const class_name& a, const class_name& b);	\


#endif // CE_VECTOR_BASE_H