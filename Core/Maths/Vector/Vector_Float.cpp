#include "Vector_Float.h"

#include <math.h>
#include "Maths/Maths.h"

Vector_Float::Vector_Float()
	: m_x(0.0f)
	, m_y(0.0f)
	, m_z(0.0f)
	, m_w(0.0f)
{
}

Vector_Float::Vector_Float(float x, float y, float z, float w)
	: m_x(x)
	, m_y(y)
	, m_z(z)
	, m_w(w)
{
}

float Vector_Float::Dot(const Vector_Float& b) const
{
	return Dot(*this, b);
}

Vector_Float Vector_Float::Cross(const Vector_Float& b) const
{
	return Cross(*this, b);
}

void Vector_Float::Normalize()
{
	*this = Normalize(*this);
}

void Vector_Float::NormalizeSafe()
{
	*this = NormalizeSafe(*this);
}

float Vector_Float::Legnth() const
{
	return Length(*this);
}

float Vector_Float::Legnth2() const
{
	return Length2(*this);
}

float Vector_Float::Distance(const Vector_Float& b) const
{
	return Distance(*this, b);
}

float Vector_Float::Distance2(const Vector_Float& b) const
{
	return Distance2(*this, b);
}

Vector_Float Vector_Float::operator-(const Vector_Float& b) const
{
	Vector_Float o(*this);
	o.m_x -= b.m_x;
	o.m_y -= b.m_y;
	o.m_z -= b.m_z;
	o.m_w -= b.m_w;
	return o;
}

Vector_Float Vector_Float::operator+(const Vector_Float& b) const
{
	Vector_Float o(*this);
	o.m_x += b.m_x;
	o.m_y += b.m_y;
	o.m_z += b.m_z;
	o.m_w += b.m_w;
	return o;
}

Vector_Float Vector_Float::operator*(const Vector_Float& b) const
{
	Vector_Float o(*this);
	o.m_x *= b.m_x;
	o.m_y *= b.m_y;
	o.m_z *= b.m_z;
	o.m_w *= b.m_w;
	return o;
}
 
Vector_Float Vector_Float::operator/(const Vector_Float& b) const
{
	Vector_Float o(*this);
	o.m_x /= b.m_x;
	o.m_y /= b.m_y;
	o.m_z /= b.m_z;
	o.m_w /= b.m_w;
	return o;
}

Vector_Float Vector_Float::operator/(const float b) const
{
	Vector_Float o(*this);
	o.m_x /= b;
	o.m_y /= b;
	o.m_z /= b;
	o.m_w /= b;
	return o;
}

Vector_Float Vector_Float::operator*(const float b) const
{
	Vector_Float o(*this);
	o.m_x *= b;
	o.m_y *= b;
	o.m_z *= b;
	o.m_w *= b;
	return o;
}

float Vector_Float::Dot(const Vector_Float& a, const Vector_Float& b)
{
	return (a.m_x * b.m_x) + (a.m_y * b.m_y) + (a.m_z * b.m_z) + (a.m_w * b.m_w);
}

Vector_Float Vector_Float::Cross(const Vector_Float& a, const Vector_Float& b)
{
	Vector_Float o;
	o.m_x = a.m_y * b.m_z - b.m_y * a.m_z;
	o.m_y = a.m_z * b.m_x - b.m_z * a.m_x;
	o.m_z = a.m_x * b.m_y - b.m_x * a.m_y;
	return o;
}

Vector_Float Vector_Float::Normalize(const Vector_Float& a)
{
	float length = Length(a);

	Vector_Float o(a);
	return o / length;
}

Vector_Float Vector_Float::NormalizeSafe(const Vector_Float& a)
{
	float length = Length(a);

	Vector_Float o(a);
	if (Maths::IsNearZero(length, 0.001f) == false)
	{
		return o / length;
	}
	return o;
}

float Vector_Float::Length(const Vector_Float& a)
{
	return sqrtf(Length2(a));
}

float Vector_Float::Length2(const Vector_Float& a)
{
	return Dot(a, a);
}

float Vector_Float::Distance(const Vector_Float& a, const Vector_Float& b)
{
	return sqrtf(Distance2(a, b));
}

float Vector_Float::Distance2(const Vector_Float& a, const Vector_Float& b)
{
	return (b - a).Legnth2();
}
