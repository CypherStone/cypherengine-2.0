#ifndef CE_GEOMETRY_H
#define CE_GEOMETRY_H

#include "Includes.h"
#include "Maths/Vector/Vec3.h"

namespace Geometry
{
	Vec3 PlaneIntersectionPoint(const Vec3& linePosition, const Vec3& lineDirection, const Vec3& planePosition, const Vec3& planeNormal)
	{
		Vec3 diff			= linePosition - planePosition;
		float directionDot	= diff.Dot(lineDirection);
		float planeDot		= lineDirection.Dot(planeNormal);
		float length		= planeDot / directionDot;

		return lineDirection - (lineDirection * length);
	}
};

#endif // CE_GEOMETRY_H