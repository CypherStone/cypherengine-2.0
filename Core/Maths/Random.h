#ifndef CE_RANDOM_H
#define CE_RANDOM_H

#include "Includes.h"

class Random
{
public:
				Random(uint32 seed = 0);

	int32		GetInt() const;
	int32		GetRangeInt(int32 min, int32 max) const;

	float		GetFloat() const;
	float		GetDecimalFloat() const;
	float		GetRangeFloat(float min, float max) const;

	uint32		GetSeed() const;
	void		SetSeed(uint32 seed);
	void		ApplySeed() const;

private:


	uint32		m_seed;
};

inline uint32 Random::GetSeed() const
{
	return m_seed;
}

inline void Random::SetSeed(uint32 seed)
{
	m_seed = seed;
}

#endif // CE_RANDOM_H