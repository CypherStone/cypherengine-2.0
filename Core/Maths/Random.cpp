#include "Random.h"

#include <stdlib.h>

Random::Random(uint32 seed)
	: m_seed(seed)
{
}

int32 Random::GetInt() const
{
	return static_cast<int32>(rand());
}

int32 Random::GetRangeInt(int32 min, int32 max) const
{
	return GetInt() % max + min;
}

float Random::GetFloat() const
{
	return static_cast<float>(GetInt());
}

float Random::GetDecimalFloat() const
{
	return GetFloat() / static_cast<float>(RAND_MAX);
}

float Random::GetRangeFloat(float min, float max) const
{
	return min + GetFloat() / static_cast<float>(RAND_MAX / (max - min));
}

void Random::ApplySeed() const
{
	srand(m_seed);
}