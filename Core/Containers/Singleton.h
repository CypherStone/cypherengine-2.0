#ifndef CE_SINGLETON_H
#define CE_SINGLETON_H

#include "Includes.h"

template<typename T>
class Singleton
{
public:

	static void			Instantiate();
	static void			Destroy();

	static T&			Get();
	static T&			LazyGet();

	static bool			IsInstantiated();

private:

	Singleton() {};
	~Singleton() {};

	static T*			ms_instance;
};

template<typename T>
inline void Singleton<T>::Instantiate()
{
	assert(IsInstantiated() == false);
	ms_instance = new T();
}

template<typename T>
inline void Singleton<T>::Destroy()
{
	delete ms_instance;
	ms_instance = nullptr;
}

template<typename T>
inline T& Singleton<T>::Get()
{
	assert(IsInstantiated() == true);
	return *ms_instance;
}

template<typename T>
inline T& Singleton<T>::LazyGet()
{
	if (IsInstantiated() == false)
	{
		Instantiate();
	}

	return Get();
}

template<typename T>
inline bool Singleton<T>::IsInstantiated()
{
	return ms_instance != nullptr;
}

#define SINGLETON_IMPLEMENTATION(x)		\
public:									\
	friend class Singleton<x>;			\
										\
private:								\
	x();								\
	~x();								\
										\
public:									\

#define GLOBAL_SINGLE_TYPEDEF(x, n)		typedef Singleton<x> n

#endif // CE_SINGLETON_H

