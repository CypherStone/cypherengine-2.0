#ifndef CE_ALGORITHMS_H
#define CE_ALGORITHMS_H

#include "Includes.h"

namespace Algorithms
{
	// Swap using a temporary variable.
	template <class T>
	void Swap(T& a, T& b)
	{
		if (&a == &b)
		{
			return;
		}

		T temp = a;
		a = b;
		b = temp;
	}

	// A simple compare function that can be used alongside the sort and search algorithms.
	// class T must implement the less and greater operators.
	template <class T>
	int32 SimpleCompare(const T& a, const T& b)
	{
		if (a > b)
		{
			return 1;
		}
		if (a < b)
		{
			return -1;
		}
		return 0;
	}

	// A simple shuffle algorithm that starts a element n - 1, and randomly swaps with any previous elements.
	// Then keep reverse iterating reducing the range.
	// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
	template <class T, class S>
	void Shuffle(T* data, S count)
	{
		for (S i = count - 1; i >= 1; --i)
		{
			S j = rand() % (i + 1);
			Swap(data[i], data[j]);
		}
	}

	namespace BubbleSort_Impl
	{
		template <class T, class S, class C>
		void Basic(T* data, S count, C compare)
		{
			bool hasSwapped;

			do
			{
				hasSwapped = false;

				for (S i = 1; i < count; ++i)
				{
					T& a = data[i - 1];
					T& b = data[i];

					if (compare(a, b) > 0)
					{
						Swap(a, b);
						hasSwapped = true;
					}
				}
			} while (hasSwapped == true);
		}

		template <class T, class S, class C>
		void PassReduction(T* data, S count, C compare)
		{
			bool hasSwapped;

			S passCount = count;

			do
			{
				hasSwapped = false;

				for (S i = 1; i < passCount; ++i)
				{
					T b = data[i];
					T a = data[i - 1];

					if (compare(a, b) > 0)
					{
						Swap(a, b);
						hasSwapped = true;
					}
				}

				--passCount;

			} while (hasSwapped == true);
		}

		template <class T, class S, class C>
		void Optimised(T* data, S count, C compare)
		{
			while (count)
			{
				S newCount = 0;

				for (S i = 1; i < count; ++i)
				{
					T& a = data[i - 1];
					T& b = data[i];

					if (compare(a, b) > 0)
					{
						Swap(a, b);
						newCount = i;
					}
				}

				count = newCount;
			}
		}
	};

	// Iterate n elements, swapping each element if it passes the comparison.
	// Keep reiterating until no elements are swapped.
	// https://en.wikipedia.org/wiki/Bubble_sort
	template <class T, class S, class C>
	void BubbleSort(T* data, S count, C compare)
	{
		BubbleSort_Impl::Optimised(data, count, compare);
	}

	namespace QS_Lomuto
	{
		template <class T, class C>
		int64 Partition(T* data, int64 low, int64 high, C compare)
		{
			T& pivotData = data[high];
			int64 i = low;

			for (int64 j = low; j < high; ++j)
			{
				T& current = data[j];
				if (compare(current, pivotData) < 0)
				{
					Swap(current, data[i++]);
				}
			}

			Swap(data[i], pivotData);

			return i;
		}

		template <class T, class C>
		void Impl(T* data, int64 low, int64 high, C compare)
		{
			if (low >= high)
			{
				return;
			}

			int64 pivot = Partition(data, low, high, compare);

			Impl(data, low, pivot - 1, compare);
			Impl(data, pivot + 1, high, compare);
		}
	};

	namespace QS_Hoare
	{
		template <class T, class C>
		int64 Partition(T* data, const int64 low, const int64 high, C compare)
		{
			T pivotData = data[(high + low) >> 1];
			int64 i = low - 1, j = high + 1;

			while (true)
			{
				while (compare(data[++i], pivotData) < 0) {}
				while (compare(data[--j], pivotData) > 0) {}

				if (i >= j)
				{
					return j;
				}

				Swap(data[i], data[j]);
			}

			return i;
		}

		template <class T, class C>
		void Impl(T* data, const int64 low, const int64 high, C compare)
		{
			if (low >= high)
			{
				return;
			}

			int64 pivot = Partition(data, low, high, compare);

			Impl(data, low, pivot, compare);
			Impl(data, pivot + 1, high, compare);
		}
	};

	// Start at the end of the data for the pivot, and keep moving the pivot and swapping the elements along until it can no longer swap.
	// Then recursively do the same tecnique to the elements each side of the pivot.
	// https://en.wikipedia.org/wiki/Quicksort
	template <class T, class S, class C>
	void QuickSort(T* data, S count, C compare)
	{
		QS_Hoare::Impl(data, int64(0), int64(count) - 1, compare);
	}

	namespace MergeSortImpl
	{
		template <class T, class S, class C>
		void Merge(T* data, T* dataCopy, S left, S pivot, S right, C compare)
		{
			S leftCount = pivot - left + 1;
			S rightCount = right - pivot;

			memcpy(dataCopy, data + left, leftCount * sizeof(T));
			memcpy(dataCopy + leftCount, data + pivot + 1, rightCount * sizeof(T));

			S i = 0, j = 0, k = left;

			while (i < leftCount && j < rightCount)
			{
				if (compare(dataCopy[i], dataCopy[leftCount + j]) <= 0)
				{
					data[k] = dataCopy[i];
					i++;
				}
				else
				{
					data[k] = dataCopy[leftCount + j];
					j++;
				}
				k++;
			}

			memcpy(data + k, dataCopy + i, (leftCount - i) * sizeof(T));
			memcpy(data + k + (leftCount - i), dataCopy + leftCount + j, (rightCount - j) * sizeof(T));
		}

		template <class T, class S, class C>
		void Recursion(T* data, T* dataCopy, S left, S right, C compare)
		{
			if (left >= right)
			{
				return;
			}

			S pivot = left + ((right - left) >> 1);
			Recursion(data, dataCopy, left, pivot, compare);
			Recursion(data, dataCopy, pivot + 1, right, compare);

			Merge(data, dataCopy, left, pivot, right, compare);
		}
	}

	// Similar to quick sort, it partitions the data until 2 elements and merges them.
	// On the way back up, it then merges all the blocks together.
	// https://en.wikipedia.org/wiki/Merge_sort
	template <class T, class S, class C>
	void MergeSort(T* data, S count, C compare)
	{
		if (count < 2)
		{
			return;
		}

		T* dataCopy = new T[count];

		MergeSortImpl::Recursion(data, dataCopy, S(0), count - 1, compare);

		delete[] dataCopy;
	}

	// A fun and dumb sorting algorithm that just keeps shuffling the data until it is in order.
	// https://en.wikipedia.org/wiki/Bogosort
	template <class T, class S, class C>
	void BogoSort(T* data, S count, C compare)
	{
		bool isSorted = false;
		while (isSorted == false)
		{
			isSorted = true;
			for (S i = 0; i < count - 1; ++i)
			{
				if (compare(data[i], data[i + 1]) > 0)
				{
					isSorted = false;
					break;
				}
			}

			Shuffle(data, count);
		}
	}

	// A simple linear search.
	template <class T, class S, class C>
	int32 LinearSearch(T* data, S count, const T& find, C compare)
	{
		for (int32 i = 0; i < count; ++i)
		{
			if (compare(find, data[i]) == 0)
			{
				return i;
			}
		}

		return -1;
	}

	// An iterative binary search (only works on sorted arrays).
	// https://en.wikipedia.org/wiki/Binary_search_algorithm
	template <class T, class S, class C>
	int32 BinarySearch(T* data, S count, const T& find, C compare)
	{
		int32 low = 0;
		int32 high = int32(count - 1);

		while (low != high)
		{
			int32 mid = (low + high) >> 1;

			if (compare(data[mid], find) > 0)
			{
				high = mid - 1;
			}
			else
			{
				low = mid;
			}

			if (data[low] == find)
			{
				return low;
			}
		}

		return -1;
	}
};

#endif // CE_ALGORITHMS_H