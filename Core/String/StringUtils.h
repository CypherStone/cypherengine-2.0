#ifndef CE_STRING_UTILS_H
#define CE_STRING_UTILS_H

#include "Includes.h"

namespace StringUtils
{
	int32 GetLength(const char* str)
	{
		if (str == nullptr)
		{
			return -1;
		}

		int32 i = 0;
		while (str[i] != '\0')
		{
			++i;
		}

		return i;
	}

	int32 FindNOf(const char* str, char find, int32 n)
	{
		if (str == nullptr)
		{
			return -1;
		}

		int32 pos = -1;
		for (int32 i = 0; str[i] != '\0'; ++i)
		{
			if (str[i] == find)
			{
				pos = i;
				if (n >= pos && n != -1)
				{
					return pos;
				}
			}
		}

		return pos;
	}

	int32 FindFirstOf(const char* str, char find)
	{
		return FindNOf(str, find, 0);
	}

	int32 FindLastOf(const char* str, char find)
	{
		return FindNOf(str, find, -1);
	}

	const char* GetFileExtension(const char* filepath)
	{
		int32 length	= GetLength(filepath);
		int32 index		= FindLastOf(filepath, '.');
		
		if (index == -1 || index + 1 >= length)
		{
			return nullptr;
		}

		return filepath + index + 1;
	}

	char* GetFileName(char* filepath)
	{
		int32 length			= GetLength(filepath);
		int32 lastSlashIndex	= FindLastOf(filepath, '\\');
		int32 extIndex			= FindLastOf(filepath, '.');

		if (lastSlashIndex + 1 >= length || extIndex + 1 >= length)
		{
			return nullptr;
		}

		// We could use Maths::Clamp here but makes a weird dependency.
		lastSlashIndex	= lastSlashIndex < 0 ? 0 : lastSlashIndex + 1;

		if (extIndex > 0)
		{
			filepath[extIndex] = '\0';
		}

		return lastSlashIndex + filepath;
	}
};

#endif // CE_STRING_UTILS_H