#ifndef CE_FSM_H
#define CE_FSM_H

#include "Includes.h"
#include <map>
#include <functional>

template<typename T>
class FSM
{
public:

	typedef	std::function<void(T)>			StateDelegate;
	typedef	std::function<void(T, T)>		ChangeStateDelegate;

											FSM(T initialValue);

	void									RegisterState(T state, const StateDelegate& del);

	void									Update();

	T										GetState() const;
	void									SetState(T state);
	void									SetChangeStateDelegate(const ChangeStateDelegate& del);

private:

	typedef std::map<T, StateDelegate>		StateMap;

	StateMap								m_states;
	ChangeStateDelegate						m_changeStateDelegate;
	T										m_state;
};

template<typename T>
inline FSM<T>::FSM(T initialValue)
	: m_state(initialValue)
{
}

template<typename T>
void FSM<T>::RegisterState(T state, const StateDelegate& del)
{
	m_states.emplace(state, del);
}

template<typename T>
void FSM<T>::Update()
{
	typename StateMap::iterator pair = m_states.find(m_state);

	if (pair == m_states.end())
	{
		return;
	}

	pair->second(m_state);
}
template<typename T>
T FSM<T>::GetState() const
{
	return m_state;
}

template<typename T>
void FSM<T>::SetState(T state)
{
	if (state == m_state)
	{
		return;
	}

	if (m_changeStateDelegate != nullptr)
	{
		m_changeStateDelegate(m_state, state);
	}

	m_state = state;
}

template<typename T>
void FSM<T>::SetChangeStateDelegate(const ChangeStateDelegate& del)
{
	m_changeStateDelegate = del;
}

#endif // CE_FSM_H

