#ifndef CE_RTTI_H
#define CE_RTTI_H

#define RTTI_BASE(className, idType)																									\
public:																																	\
	typedef idType id;																													\
	virtual id		GetClassId() const	{ return ms_##classNameId; }																	\
	static id		GetStaticClassId()	{ return ms_##classNameId; }																	\
	bool			IsClass(id t) const	{ return t == GetClassId(); }																	\
	template<typename T> T* AsClass()	{ return IsClass(T::GetStaticClassId()) ? static_cast<T*>(this) : nullptr; }					\
	template<typename T> const T* AsClass() const { return IsClass(T::GetStaticClassId()) ? static_cast<const T*>(this) : nullptr; }	\
private:																																\
	static id ms_##classNameId																											\

#define RTTI_DERIVED(className, bassClass)																								\
public:																																	\
	virtual bassClass::id	GetClassId() const override { return ms_##classNameId; }													\
	static bassClass::id	GetStaticClassId() { return ms_##classNameId; }																\
	typedef className bassClass;																										\
private:																																\
	static bassClass::id ms_##classNameId																								\

#define INITIALISE_RTTI(className, v) className::id className::ms_##classNameId = v

#endif // CE_RTTI_H