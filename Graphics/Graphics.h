#ifndef CE_GRAPHICS_H
#define CE_GRAPHICS_H

#include "Includes.h"

#define CE_DRIVER_D3D12		CE_WIN64
#define CE_DRIVER_VULKAN	CE_WIN64

#if CE_WIN64
#define NUM_BACK_BUFFERS 2
#else
#define NUM_BACK_BUFFERS 1
#endif // CE_WIN64

#if CE_DRIVER_D3D12
#define CE_DRIVER_D3D12_ONLY(x) x
#else
#define CE_DRIVER_D3D12_ONLY(x)
#endif // !CE_DRIVER_D3D12

#if CE_DRIVER_VULKAN
#define CE_DRIVER_VULKAN_ONLY(x) x	
#else
#define CE_DRIVER_VULKAN_ONLY(x)
#endif // !CE_DRIVER_VULKAN

enum GraphicDriver
{
	GD_Unknown,
#if CE_DRIVER_D3D12
	GD_Direct3D12,
#endif // CE_DRIVER_D3D12
#if CE_DRIVER_VULKAN
	GD_Vulkan,
#endif // CE_DRIVER_VULKAN

	GD_Total
};

#define CE_MULTI_DRIVER	(CE_DRIVER_D3D12 + CE_DRIVER_VULKAN) > 1

// TODO:
// Complete support for singular driver.
// Typedef to base driver name, don't implement driver derived classes, use macro implementation.

#if CE_MULTI_DRIVER
#define MD_VIRTUAL			virtual
#define MD_PURE_VIRTUAL		= 0
#define MD_ONLY(x)			x
#define MD_OVERRIDE			override
#else
#define MD_VIRTUAL	
#define MD_PURE_VIRTUAL
#define MD_ONLY(x)
#define MD_OVERRIDE
#endif // !CE_MULTI_DRIVER

#endif // CE_GRAPHICS_H