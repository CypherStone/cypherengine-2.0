#include "Driver.h"

MD_ONLY(INITIALISE_RTTI(Driver, GraphicDriver::GD_Unknown));

#include "Adapters/Adapter.h"
#include "Driver_D3D12.h"

Driver* g_Driver;

GraphicDriver Driver::GetDefaultDriver()
{
#if CE_WIN64 && CE_DRIVER_D3D12
	return GraphicDriver::GD_Direct3D12;
#else
	return GraphicDriver::GD_Unknown;
#endif
}

Driver* Driver::Create(const Adapter* adapter, const Window* window)
{
	if (g_Driver != nullptr)
	{
		return g_Driver;
	}

	Driver* driver = nullptr;

#if CE_DRIVER_D3D12
	if (adapter->IsClass(GraphicDriver::GD_Direct3D12))
	{
		driver = new DriverD3D12();
	}
#endif // CE_DRIVER_D3D12

	assert(driver != nullptr);

	if (driver != nullptr && driver->Setup(adapter, window) == false)
	{
		driver->Destroy();

		delete driver;
		driver = nullptr;
	}

	g_Driver = driver;
	   
	return g_Driver;
}
