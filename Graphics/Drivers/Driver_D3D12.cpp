#include "Driver_D3D12.h"

#if CE_DRIVER_D3D12

#include "Adapters/Adapter.h"
#include "Adapters/Adapter_D3D12.h"
#include "Memory/Memory.h"
#include "Window/Window.h"

MD_ONLY(INITIALISE_RTTI(DriverD3D12,GraphicDriver::GD_Direct3D12));

DriverD3D12::DriverD3D12()
	: m_device(nullptr)
	, m_swapChain(nullptr)
    , m_pd3dRtvDescHeap(nullptr)
    , m_pd3dSrvDescHeap(nullptr)
    , m_pd3dCommandList(nullptr)
    , m_pd3dCommandQueue(nullptr)
    , m_fence(nullptr)
    , m_fenceEvent(nullptr)
    , m_fenceLastSignaledValue(0)
    , m_frameInFlight(0)
{
    ceMemClear(m_frameContext, sizeof(FrameContext) * NUM_FRAMES_IN_FLIGHT);

    ceMemClear(m_mainRenderTargetResource, sizeof(ID3D12Resource) * NUM_BACK_BUFFERS);
    ceMemClear(m_mainRenderTargetDescriptor, sizeof(D3D12_CPU_DESCRIPTOR_HANDLE) * NUM_BACK_BUFFERS);
}

DriverD3D12::~DriverD3D12()
{
}

bool DriverD3D12::Setup(const Adapter* adapter, const Window* window)
{
    const AdapterD3D12* adapterD3D12 = adapter->AsClass<AdapterD3D12>();

    HRESULT result = D3D12CreateDevice(adapterD3D12->GetAdapter(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_device));
    if (result != S_OK)
    {
        return false;
    }

    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = { 0 };
    swapChainDesc.BufferCount = NUM_BACK_BUFFERS;
    swapChainDesc.Width = window->GetWidth();
    swapChainDesc.Height = window->GetHeight();
    swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
    swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
    swapChainDesc.Stereo = false;

    D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
    heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
    heapDesc.NumDescriptors = NUM_BACK_BUFFERS;
    heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    heapDesc.NodeMask = 1;
    if (m_device->CreateDescriptorHeap(&heapDesc , IID_PPV_ARGS(&m_pd3dRtvDescHeap)) != S_OK)
    {
        return false;
    }

    size_t rtvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = m_pd3dRtvDescHeap->GetCPUDescriptorHandleForHeapStart();
    for (uint8 i = 0; i < NUM_BACK_BUFFERS; i++)
    {
        m_mainRenderTargetDescriptor[i] = rtvHandle;
        rtvHandle.ptr += rtvDescriptorSize;
    }

    D3D12_DESCRIPTOR_HEAP_DESC heapUavDesc = {};
    heapUavDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    heapUavDesc.NumDescriptors = 1;
    heapUavDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    if (m_device->CreateDescriptorHeap(&heapUavDesc, IID_PPV_ARGS(&m_pd3dSrvDescHeap)) != S_OK)
    {
        return false;
    }

    D3D12_COMMAND_QUEUE_DESC commandQueueDesc = {};
    commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
    commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    commandQueueDesc.NodeMask = 1;
    if (m_device->CreateCommandQueue(&commandQueueDesc, IID_PPV_ARGS(&m_pd3dCommandQueue)) != S_OK)
    {
        return false;
    }

    for (uint8 i = 0; i < NUM_FRAMES_IN_FLIGHT; i++)
    {
        if (m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_frameContext[i].CommandAllocator)) != S_OK)
        {
            return false;
        }
    }

    if (m_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_frameContext[0].CommandAllocator, nullptr, IID_PPV_ARGS(&m_pd3dCommandList)) != S_OK || m_pd3dCommandList->Close() != S_OK)
    {
        return false;
    }

    if (m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence)) != S_OK)
    {
        return false;
    }

    m_fenceEvent = CreateEvent(nullptr, false, false, nullptr);
    if (m_fenceEvent == nullptr)
    {
        return false;
    }

    IDXGIFactory4* dxgiFactory = nullptr;
    IDXGISwapChain1* swapChain1 = nullptr;
    if (CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory)) != S_OK ||
        dxgiFactory->CreateSwapChainForHwnd(m_pd3dCommandQueue, window->GetHandle(), &swapChainDesc, nullptr, nullptr, &swapChain1) != S_OK ||
        swapChain1->QueryInterface(IID_PPV_ARGS(&m_swapChain)) != S_OK)
    {
        return false;
    }

    swapChain1->Release();
    dxgiFactory->Release();
    m_swapChain->SetMaximumFrameLatency(NUM_BACK_BUFFERS);
    m_hSwapChainWaitableObject = m_swapChain->GetFrameLatencyWaitableObject();

    for (uint8 i = 0; i < NUM_BACK_BUFFERS; i++)
    {
        ID3D12Resource* pBackBuffer = nullptr;
        m_swapChain->GetBuffer(i, IID_PPV_ARGS(&pBackBuffer));
        m_device->CreateRenderTargetView(pBackBuffer, nullptr, m_mainRenderTargetDescriptor[i]);
        m_mainRenderTargetResource[i] = pBackBuffer;
    }

	return true;
}

void DriverD3D12::StartFrame()
{
    FrameContext* frameCtxt = WaitForNextFrameResources();
    uint32 backBufferIdx = m_swapChain->GetCurrentBackBufferIndex();
    frameCtxt->CommandAllocator->Reset();

    D3D12_RESOURCE_BARRIER barrier = {};
    barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    barrier.Transition.pResource = m_mainRenderTargetResource[backBufferIdx];
    barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;

    float clearColor[4]{ 0.3f, 0.3f, 1.0f, 1.0f };

    m_pd3dCommandList->Reset(frameCtxt->CommandAllocator, nullptr);
    m_pd3dCommandList->ResourceBarrier(1, &barrier);
    m_pd3dCommandList->OMSetRenderTargets(1, &m_mainRenderTargetDescriptor[backBufferIdx], false, nullptr);
    m_pd3dCommandList->ClearRenderTargetView(m_mainRenderTargetDescriptor[backBufferIdx], clearColor, 0, nullptr);
    m_pd3dCommandList->SetDescriptorHeaps(1, &m_pd3dSrvDescHeap);
}

void DriverD3D12::EndFrame()
{
    uint32 backBufferIdx = m_swapChain->GetCurrentBackBufferIndex();

    D3D12_RESOURCE_BARRIER barrier = {};
    barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    barrier.Transition.pResource = m_mainRenderTargetResource[backBufferIdx];
    barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;

    m_pd3dCommandList->ResourceBarrier(1, &barrier);
    m_pd3dCommandList->Close();

    m_pd3dCommandQueue->ExecuteCommandLists(1, (ID3D12CommandList* const*)&m_pd3dCommandList);

    FrameContext* frameCtxt = &m_frameContext[m_frameInFlight];

    m_swapChain->Present(1, 0); // Present with vsync
    //m_swapChain->Present(0, 0); // Present without vsync

    uint64 fenceValue = m_fenceLastSignaledValue + 1;
    m_pd3dCommandQueue->Signal(m_fence, fenceValue);
    m_fenceLastSignaledValue = fenceValue;
    frameCtxt->FenceValue = fenceValue;
}

DriverD3D12::FrameContext* DriverD3D12::WaitForNextFrameResources()
{
    m_frameInFlight = (m_frameInFlight + 1) % NUM_FRAMES_IN_FLIGHT;

    HANDLE waitableObjects[] = { m_hSwapChainWaitableObject, nullptr };
    DWORD numWaitableObjects = 1;

    FrameContext* frameCtxt = &m_frameContext[m_frameInFlight];
    uint64 fenceValue = frameCtxt->FenceValue;
    if (fenceValue != 0) // means no fence was signaled
    {
        frameCtxt->FenceValue = 0;
        m_fence->SetEventOnCompletion(fenceValue, m_fenceEvent);
        waitableObjects[1] = m_fenceEvent;
        numWaitableObjects = 2;
    }

    WaitForMultipleObjects(numWaitableObjects, waitableObjects, TRUE, INFINITE);

    return frameCtxt;
}

void DriverD3D12::Destroy()
{
    if (m_device != nullptr)
    {
        m_device->Release();
        m_device = nullptr;
    }
}

#endif // CE_DRIVER_D3D12