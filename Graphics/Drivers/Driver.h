#ifndef CE_DRIVER_H
#define CE_DRIVER_H

#include "Includes.h"

#include "Graphics.h"
#include "Model/RTTI.h"

class Adapter;
class Window;

class Driver
{
	MD_ONLY(RTTI_BASE(Driver, GraphicDriver));
public:
	MD_VIRTUAL				~Driver() { }

	MD_VIRTUAL bool			Setup(const Adapter* adapter, const Window* window) MD_PURE_VIRTUAL;

	MD_VIRTUAL void			StartFrame() MD_PURE_VIRTUAL;
	MD_VIRTUAL void			EndFrame() MD_PURE_VIRTUAL;

	MD_VIRTUAL void			Destroy() MD_PURE_VIRTUAL;
	
	static GraphicDriver	GetDefaultDriver();
	static Driver*			Create(const Adapter* adapter, const Window* window);

private:

};

extern Driver* g_Driver;

#endif // CE_DRIVER_H