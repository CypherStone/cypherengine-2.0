#ifndef CE_D3D12_DEFINES_H
#define CE_D3D12_DEFINES_H

#include "Drivers/Driver.h"

#if CE_DRIVER_D3D12

#include <d3d12.h>
#include <dxgi1_4.h>
#include <D3Dcompiler.h>

typedef void*	HANDLE;

#endif // CE_DRIVER_D3D12

#endif // CE_DriverD3D12_H

