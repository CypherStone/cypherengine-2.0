#ifndef CE_DRIVER_D3D12_H
#define CE_DRIVER_D3D12_H

#include "Driver.h"

#if CE_DRIVER_D3D12

#include "Graphics.h"
#include "D3D12_Defines.h"

class Adapter;

#define NUM_FRAMES_IN_FLIGHT 3

class DriverD3D12 : public Driver
{
	MD_ONLY(RTTI_DERIVED(DriverD3D12,Driver));

public:
							DriverD3D12();
	MD_VIRTUAL				~DriverD3D12();

	MD_VIRTUAL bool			Setup(const Adapter* adapter, const Window* window);

	MD_VIRTUAL void			StartFrame() MD_OVERRIDE;
	MD_VIRTUAL void			EndFrame() MD_OVERRIDE;
	
	MD_VIRTUAL void			Destroy() MD_OVERRIDE;

	ID3D12Device*				GetDevice() const;
	ID3D12GraphicsCommandList*	GetCommandList() const;
	ID3D12DescriptorHeap*		GetDescriptorHeap() const;

private:

	struct FrameContext
	{
		ID3D12CommandAllocator* CommandAllocator;
		uint64                  FenceValue;
	};

	FrameContext*					WaitForNextFrameResources();

	FrameContext					m_frameContext[NUM_FRAMES_IN_FLIGHT];

	D3D12_CPU_DESCRIPTOR_HANDLE		m_mainRenderTargetDescriptor[NUM_BACK_BUFFERS];

	ID3D12Resource*					m_mainRenderTargetResource[NUM_BACK_BUFFERS];

	ID3D12Device*					m_device;
	IDXGISwapChain3*				m_swapChain;

	ID3D12DescriptorHeap*			m_pd3dRtvDescHeap;
	ID3D12DescriptorHeap*			m_pd3dSrvDescHeap;
	ID3D12GraphicsCommandList*		m_pd3dCommandList;
	ID3D12CommandQueue*				m_pd3dCommandQueue;
	ID3D12Fence*					m_fence;
	HANDLE							m_fenceEvent;
	HANDLE							m_hSwapChainWaitableObject;
	uint64							m_fenceLastSignaledValue;
	uint8							m_frameInFlight;
};

inline ID3D12Device* DriverD3D12::GetDevice() const
{
	return m_device;
}

inline ID3D12GraphicsCommandList* DriverD3D12::GetCommandList() const
{
	return m_pd3dCommandList;
}

inline ID3D12DescriptorHeap* DriverD3D12::GetDescriptorHeap() const
{
	return m_pd3dSrvDescHeap;
}

#endif // CE_DRIVER_D3D12

#endif // CE_DRIVER_D3D12_H
