#ifndef CE_ADAPTER_D3D12_H
#define CE_ADAPTER_D3D12_H

#include "Includes.h"

#include "Graphics.h"

#if CE_DRIVER_D3D12

#include "Model/RTTI.h"

#include "Adapter.h"
#include "Drivers/D3D12_Defines.h"

class AdapterD3D12 : public Adapter
{
	RTTI_DERIVED(AdapterD3D12, Adapter);
public:
						AdapterD3D12(IDXGIAdapter1* adapter);
						~AdapterD3D12();

	virtual void		Setup() override;

	IDXGIAdapter1*		GetAdapter() const;

private:

	IDXGIAdapter1*		m_adapter;
};

inline IDXGIAdapter1* AdapterD3D12::GetAdapter() const
{
	return m_adapter;
}

#endif // CE_DRIVER_D3D12

#endif // CE_ADAPTER_D3D12_H