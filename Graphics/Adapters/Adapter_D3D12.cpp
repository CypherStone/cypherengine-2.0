#include "Adapter_D3D12.h"

#if CE_DRIVER_D3D12

#include <stdio.h>

#include <d3d12.h>
#include <dxgi1_4.h>

#include "Memory/Memory.h"

INITIALISE_RTTI(AdapterD3D12, GraphicDriver::GD_Direct3D12);

AdapterD3D12::AdapterD3D12(IDXGIAdapter1* adapter)
	: m_adapter(adapter)
{
}

AdapterD3D12::~AdapterD3D12()
{
	m_adapter->Release();
}

void AdapterD3D12::Setup()
{
	DXGI_ADAPTER_DESC1 description;
	m_adapter->GetDesc1(&description);

	if (description.VendorId == NVIDIA_VENDOR_ID)
	{
		m_vendor = Vendors::V_NVIDIA;
	}
	else if (description.VendorId == AMD_VENDOR_ID)
	{
		m_vendor = Vendors::V_AMD;
	}
	else if (description.VendorId == INTEL_VENDOR_ID)
	{
		m_vendor = Vendors::V_INTEL;
	}
	else  if (description.VendorId == MICROSOFT_VENDOR_ID)
	{
		m_vendor = Vendors::V_MICROSOFT;
	}
	else
	{
		assert(false);
	}

	sprintf_s(m_adapterName, "%ws", description.Description);
}

#endif // CE_DRIVER_D3D12