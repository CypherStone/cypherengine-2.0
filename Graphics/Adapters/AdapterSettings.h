#ifndef CE_ADAPTER_SETTINGS_H
#define CE_ADAPTER_SETTINGS_H

struct AdapterSettings
{
	AdapterSettings()
		: vsync(false)
	{
	}

	bool		vsync : 1;
};

#endif // CE_ADAPTER_SETTINGS_H