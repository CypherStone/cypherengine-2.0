#ifndef CE_ADAPTER_H
#define CE_ADAPTER_H

#include "Includes.h"

#include "Model/RTTI.h"

#include "AdapterSettings.h"
#include "Graphics.h"
#include "Vendor.h"

class Adapter
{
	RTTI_BASE(Adapter, GraphicDriver);

public:

							Adapter();

	virtual void			Setup() = 0;

	const AdapterSettings&	GetSettings() const;

	const char*				GetAdapterName() const;
	Vendors					GetVendor() const;	
	uint32					GetVersion() const;

protected:

	AdapterSettings		m_settings;

	char				m_adapterName[32];
	Vendors				m_vendor;
	uint32				m_version;
};

inline const AdapterSettings& Adapter::GetSettings() const
{
	return m_settings;
}

inline const char* Adapter::GetAdapterName() const
{
	return m_adapterName;
}

inline Vendors Adapter::GetVendor() const
{
	return m_vendor;
}

inline uint32 Adapter::GetVersion() const
{
	return m_version;
}

#endif // CE_ADAPTER_MANAGER_H

