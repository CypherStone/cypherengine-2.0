#ifndef CE_VENDOR_H
#define CE_VENDOR_H

#include "Includes.h"

#define NVIDIA_VENDOR_ID		0x10DE
#define AMD_VENDOR_ID			0x1002
#define INTEL_VENDOR_ID			0x8086
#define MICROSOFT_VENDOR_ID		0x1414

enum class Vendors
{
	V_UNKNOWN,
#if CE_WIN64
	V_NVIDIA,
	V_AMD,
	V_INTEL,
	V_MICROSOFT,
#endif // CE_WIND64

	V_TOTAL
};

#endif // CE_VENOR_H