#include "AdapterManager.h"

#include "Adapter.h"

#if CE_DRIVER_D3D12
#include <d3d12.h>
#include <dxgi1_4.h>

#include "Adapter_D3D12.h"
#endif // CE_DRIVER_D3D12

AdapterManager* ADAPTERMANAGER_S::ms_instance = nullptr;

AdapterManager::AdapterManager()
    : m_mainAdapter(nullptr)
{
}

AdapterManager::~AdapterManager()
{
    assert(m_mainAdapter == nullptr);
}

void AdapterManager::Setup(GraphicDriver driver)
{
    Shutdown();

	assert(driver != GraphicDriver::GD_Unknown);

	switch (driver)
	{
#if CE_DRIVER_D3D12
	case GraphicDriver::GD_Direct3D12:
		SetupD3D12();
		break;
#endif // CE_DRIVER_D3D12
#if CE_DRIVER_VULKAN
	case GraphicDriver::GD_Vulkan:
		SetupVulkan();
		break;
#endif // CE_DRIVER_VULKAN
	default:
		assert(false);
		break;
	}

    for (Adapter* adapter : m_adapters)
    {
        adapter->Setup();
    }

    if (m_adapters.size() > 0)
    {
        m_mainAdapter = m_adapters[0];
    }
}

void AdapterManager::Shutdown()
{
    for (Adapter* adapter : m_adapters)
    {
        delete adapter;
    }

    m_mainAdapter = nullptr;
    m_adapters.clear();
}

#if CE_DRIVER_D3D12
void AdapterManager::SetupD3D12()
{
    // https://docs.microsoft.com/en-us/windows/win32/api/d3d12/nf-d3d12-d3d12createdevice

    IDXGIFactory4* dxgiFactory;
    HRESULT result = CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory));
    if (result != S_OK)
    {
        return;
    }

    for (uint32 adapterIndex = 0; ; ++adapterIndex)
    {
        IDXGIAdapter1* dxgiAdapter = nullptr;
        if (DXGI_ERROR_NOT_FOUND == dxgiFactory->EnumAdapters1(adapterIndex, &dxgiAdapter))
        {
            break;
        }

        result = D3D12CreateDevice(dxgiAdapter, D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr);
        if (SUCCEEDED(result))
        {
            m_adapters.push_back(new AdapterD3D12(dxgiAdapter));
        }
    }

    dxgiFactory->Release();
}
#endif // CE_DRIVER_D3D12

#if CE_DRIVER_VULKAN
void AdapterManager::SetupVulkan()
{
}
#endif // CE_DRIVER_VULKAN