#ifndef CE_ADAPTER_MANAGER_H
#define CE_ADAPTER_MANAGER_H

#include <vector>

#include "Includes.h"
#include "Containers/Singleton.h"

#include "Graphics.h"
#include "Vendor.h"

class Adapter;

class AdapterManager
{
	SINGLETON_IMPLEMENTATION(AdapterManager)

public:
	typedef std::vector<Adapter*>	Adapters;

	void						Setup(GraphicDriver driver);
	void						Shutdown();

	const Adapter*				GetMainAdapter() const;
	const Adapters&				GetAdapters() const;

private:

#if CE_DRIVER_D3D12
	void						SetupD3D12();
#endif // CE_DRIVER_D3D12
#if CE_DRIVER_VULKAN
	void						SetupVulkan();
#endif // CE_DRIVER_VULKAN

	Adapter*					m_mainAdapter;	
	Adapters					m_adapters;
};

inline const Adapter* AdapterManager::GetMainAdapter() const
{
	return m_mainAdapter;
}

inline const AdapterManager::Adapters& AdapterManager::GetAdapters() const
{
	return m_adapters;
}

GLOBAL_SINGLE_TYPEDEF(AdapterManager, AdapterManagerSingleton);

#define ADAPTERMANAGER		AdapterManagerSingleton::Get()
#define ADAPTERMANAGER_S	AdapterManagerSingleton

#endif // CE_ADAPTER_MANAGER_H