#include "Adapter.h"
#include "Memory/Memory.h"

INITIALISE_RTTI(Adapter, GraphicDriver::GD_Unknown);

Adapter::Adapter()
	: m_vendor(Vendors::V_UNKNOWN)
	, m_version(0)
{
	ceMemClear(&m_adapterName, sizeof(m_adapterName));
}