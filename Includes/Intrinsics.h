#ifndef CE_INTRINSICS_H
#define CE_INTRINSICS_H

#include <immintrin.h>

// Reference: https://software.intel.com/sites/landingpage/IntrinsicsGuide/#techs=SSE,SSE2,SSE3,SSSE3,SSE4_1,SSE4_2

typedef __m128		float128;
typedef __m128i		int128;
typedef __m128d		double128;

namespace Intrinsics
{
	inline float128 Set(float a)
	{
		return _mm_set_ps1(a);
	}

	inline float128 SetAll(float a, float b, float c, float d)
	{
		return _mm_set_ps(a, b, c, d);
	}

	inline float ToFloat(const float128& a)
	{
		float result;
		_mm_store_ss(&result, a);
		return result;
	}

	inline float128 Add(const float128& a, const float128& b)
	{
		return _mm_add_ps(a, b);
	}

	inline float128 Subtract(const float128& a, const float128& b)
	{
		return _mm_sub_ps(a, b);
	}

	inline float128 Multiply(const float128& a, const float128& b)
	{
		return _mm_mul_ps(a, b);
	}

	inline float128 Divide(const float128& a, const float128& b)
	{
		return _mm_div_ps(a, b);
	}

	inline float128 Round(const float128& a)
	{
		return _mm_round_ps(a, _MM_FROUND_TO_NEG_INF);
	}

	inline float128 Modulus(const float128& a, const float128& b)
	{
		float128 division	= _mm_div_ps(a, b);
		float128 decimal	= _mm_sub_ps(division, Round(division));

		return _mm_mul_ps(decimal, b);
	}

	inline float128 Sqrt(const float128& a)
	{
		return _mm_sqrt_ps(a);
	}

	inline float Dot(const float128& a, const float128& b)
	{
		float128 dotResult = _mm_dp_ps(a, b, 0xff);

		float result;
		_mm_store_ss(&result, dotResult);

		return result;
	}

	inline float Length(const float128& a)
	{
		float128 dotResult = _mm_dp_ps(a, a, 0xff);
		float128 squareDot = _mm_sqrt_ps(dotResult);

		float result;
		_mm_store_ss(&result, squareDot);

		return result;
	}

	inline float Distance(const float128& a, const float128& b)
	{
		float128 result = _mm_sub_ps(a, b);
		return Length(result);
	}

	inline float Distance2(const float128& a, const float128& b)
	{
		float128 result = _mm_sub_ps(a, b);
		return Dot(result, result);
	}

	inline float128 Cross(const float128& a, const float128& b)
	{
		// (a3, a0, a2, a1) * (b3, b1, b0, b2) - (a3, a1, a0, a2) * (b3, b0, b2, b1)
		return _mm_sub_ps(_mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 0, 2, 1)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 1, 0, 2))),
						_mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 1, 0, 2)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 0, 2, 1))));
	}

	inline void Store(const float128& a, float* arr)
	{
		_mm_store_ps(arr, a);
	}
}

#endif // CE_INTRINSICS_H

