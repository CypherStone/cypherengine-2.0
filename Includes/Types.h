#ifndef CE_TYPES_H
#define CE_TYPES_H

#include "Platforms.h"

#include <stdint.h>

#if CE_WIN64

typedef int8_t						int8;
typedef uint8_t						uint8;
typedef unsigned char				uchar;

typedef short						int16;
typedef unsigned short				uint16;

typedef int							int32;
typedef unsigned int				uint32;

typedef __int64						int64;
typedef unsigned __int64			uint64;

struct HINSTANCE__;
typedef HINSTANCE__* HINSTANCE;

struct HWND__;
typedef HWND__* HWND;

#endif // CE_WIN64

#endif // CE_TYPES_H