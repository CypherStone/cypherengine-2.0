#ifndef CE_PLATFORMS_H
#define CE_PLATFORMS_H

#define CE_WIN64		1	
#define CE_ANDROID		0

#if CE_WIN64
#define CE_WIN64_ONLY(x) x
#define CE_WIN64_SWITCH(x, y) x
#else
#define CE_WIN64_ONLY(x)
#define CE_WIN64_SWITCH(x, y) y
#endif // !CE_WIN64

#endif // CE_PLATFORMS_H