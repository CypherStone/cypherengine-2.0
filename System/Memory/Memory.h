#ifndef CE_MEMORY_H
#define CE_MEMORY_H

#include "Includes.h"

#if CE_WIN64

#include <string.h>

#define ceMemSet(x,y,z)			memset(x, y, z)
#define ceMemClear(x, y)		memset(x, 0, y)

#endif // CE_WIN64

#endif // CE_MEMORY_H