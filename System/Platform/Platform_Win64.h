#ifndef CE_PLATFORM_WIN64_H
#define CE_PLATFORM_WIN64_H

#include "Includes.h"

#if CE_WIN64

class PlatformWin64
{
	PLATFORM_IMPLEMENTATION(PlatformWin64);

	static HINSTANCE			GetInstance();

private:

	static HINSTANCE			ms_instance;
};

#endif // CE_WIN64

#endif // CE_PLATFORM_WIN64_H