#include "Platform/Platform.h"

#ifdef CE_WIN64

#include "Platform/PlatformSDK.h"

HINSTANCE Platform::ms_instance = nullptr;

void Platform::Initialise()
{
	ms_instance = (HINSTANCE)GetModuleHandle(NULL);
}

const char* Platform::GetPlatformName()
{
	return "Win64";
}

HINSTANCE Platform::GetInstance()
{
	return ms_instance;
}

#endif // CE_WIN64