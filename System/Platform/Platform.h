#ifndef CE_PLATFORM_H
#define CE_PLATFORM_H

#include "Includes.h"

#define PLATFORM_IMPLEMENTATION(x)								\
public:															\
	static void					Initialise();					\
																\
	static const char*			GetPlatformName();				\
	static const char*			GetOSVersionFriendlyName();		\
																\
private:														\
								x();							\
								~x();							\
																\
public:															\

#if CE_WIN64

#include "Platform_Win64.h"
typedef PlatformWin64 Platform;

#elif  CE_ANDROID

#include "Platform_Android.h"
typedef PlatformAndroid Platform;

#endif // CE_ANDROID

#endif // CE_PLATFORM_H