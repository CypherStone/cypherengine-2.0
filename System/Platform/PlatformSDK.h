#ifndef CE_PLATFORM_SDK_H
#define CE_PLATFORM_SDK_H

#include "Includes.h"

#ifdef CE_WIN64

#define WIN32_LEAN_AND_MEAN

#include <SDKDDKVer.h>
#include <windows.h>

#endif // CE_WIN64

#endif // CE_PLATFORM_SDK_H