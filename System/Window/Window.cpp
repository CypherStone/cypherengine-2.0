#include "Window.h"

Window::Window(uint32 width, uint32 height)
    : m_width(width)
    , m_height(height)
    , m_isFullscreen(false)
    , m_isClosed(false)
    , m_handle(INITIAL_HANDLE_VALUE)
{

}

Window::~Window()
{

}