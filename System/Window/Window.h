#ifndef CE_WINDOW_H
#define CE_WINDOW_H

#include "Includes.h"
#include "WindowHandle.h"

class Window
{
public:
	
	static Window*		Create(const char* title, uint32 width, uint32 height);
	static void			Destroy(Window* window);

	void				Process();

	WindowHandle		GetHandle() const;

	uint32				GetWidth() const;
	uint32				GetHeight() const;

	bool				IsClosed() const;

private:
						Window(uint32 width, uint32 height);
						~Window();

	WindowHandle		m_handle;

	uint32				m_width;
	uint32				m_height;
	bool				m_isFullscreen;
	bool				m_isClosed;
};

inline WindowHandle Window::GetHandle() const
{
	return m_handle;
}

inline uint32 Window::GetWidth() const
{
	return m_width;
}

inline uint32 Window::GetHeight() const
{
	return m_height;
}

inline bool Window::IsClosed() const
{
	return m_isClosed;
}

#endif // CE_WINDOW_H