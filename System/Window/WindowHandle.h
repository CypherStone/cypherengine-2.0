#ifndef CE_WINDOW_HANDLE_H
#define CE_WINDOW_HANDLE_H

#include "Includes.h"

#if CE_WIN64
typedef HWND	WindowHandle;
#define INITIAL_HANDLE_VALUE nullptr
#endif // CE_WIN64

#endif // CE_WINDOW_H