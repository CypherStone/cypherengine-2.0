#include "Window/Window.h"

#ifdef CE_WIN64

#include "Memory/Memory.h"
#include "Platform/Platform.h"
#include "Platform/PlatformSDK.h"

#include <stdlib.h>

#define WND_CLASS_NAME          L"CypherEngine"

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

ATOM RegisterClass()
{
    WNDCLASSEXW wcex;
    ceMemClear(&wcex, sizeof(WNDCLASSEX));

    wcex.cbSize         = sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = Platform::GetInstance();
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszClassName  = WND_CLASS_NAME;

    return RegisterClassExW(&wcex);
}

Window* Window::Create(const char* title, uint32 width, uint32 height)
{
    RegisterClass();

    Window* window      = new Window(width, height);
    window->m_handle    = nullptr;

    // Convert to a wide string.
    size_t length           = strlen(title) + 1;
    wchar_t* wcTitle        = new wchar_t[length];
    size_t convertedChars   = 0;
    mbstowcs_s(&convertedChars, wcTitle, length, title, _TRUNCATE);

    window->m_handle = CreateWindowW(WND_CLASS_NAME, wcTitle, WS_OVERLAPPEDWINDOW, 0, 0, width, height, nullptr, nullptr, Platform::GetInstance(), nullptr);

    if (window->m_handle == nullptr)
    {
        delete window;
        return nullptr;
    }

    ShowWindow(window->m_handle, true);
    UpdateWindow(window->m_handle);

    return window;
}

void Window::Destroy(Window* window)
{
    delete window;
}

void Window::Process()
{
    MSG msg;
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        switch (msg.message)
        {
        case WM_QUIT:
            m_isClosed = true;
            break;
        }

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}

#endif // CE_WIND64