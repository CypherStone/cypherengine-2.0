#ifndef SYSTEM_UTILS_H
#define SYSTEM_UTILS_H

class Utils
{
public:

	template <typename T>
	static void SafeRelease(T*& a);
};

template<typename T>
inline void Utils::SafeRelease(T*& a)
{
	if (a != nullptr)
	{
		a->Release();
		a = nullptr;
	}
}

#endif // UTILS_H